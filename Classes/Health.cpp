#include "Health.h"

Health::Health()
	: name("Jon"), /*board{'1', '2', '3'},*/ hitpoints(10)  // initializer list
{
	std::cout << "Hitpoint uninitialized: " << hitpoints << std::endl;

	std::cout << "Name: \"" << name << "\"" << std::endl;

	//hitpoints = 10;
	//name = "Jon";
	//board = {'1', '2', '3'};
	board = new char[3]{ '1', '2', '3' };
}

Health::~Health()
{
	delete[] board;
}

void Health::Damage(int amount)
{
	hitpoints -= amount;
}

void Health::Heal(int amount)
{
	hitpoints += amount;
}

int Health::GetHitpoints()
{
	return hitpoints;
}