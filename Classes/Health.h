#pragma once

#include <iostream>
#include <string>


class Health
{
	// In a class: Function/methods and variables
public:
	std::string name;

	//char board[3];
	char* board;

	Health();

	~Health();

	void Damage(int amount);

	void Heal(int amount);

	int GetHitpoints();

private:
	int hitpoints;

};  // C++ requires a semi-colon after the class definition