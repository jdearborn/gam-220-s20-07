#include <iostream>
#include <string>
#include "Health.h"
//#include "Player.h"
using namespace std;

class MyClass
{
	float a;
};

/*class MyClass
{
	string hjkl;
};*/

int main(int argc, char* argv[])
{
	MyClass myObject;


	// C#, this is the ONLY way:
	// Health myHealth = new Health();

	// Allocating on the "stack".  This is automatically cleaned up when the function returns.
	int myNum;
	Health myHealth;


	// Allocating on the "heap" (RAM).  
	int* otherNumber = new int();
	Health* yourHealth = new Health();


	// Accessing members of stack-allocated object:
	myHealth.Damage(4);
	myHealth.Heal(2);


	// Accessing members of heap-allocated object:
	(*yourHealth).Damage(7);  // This is nasty!!
	yourHealth->Heal(5);  // A little "syntactic sugar" to make it easier to dereference and access a member




	// Explicitly free/delete the memory we allocated
	delete otherNumber;
	delete yourHealth;

	return 0;
}