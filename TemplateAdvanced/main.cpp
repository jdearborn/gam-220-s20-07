#include <iostream>
using namespace std;

template<typename U, typename V>
class Pair
{
public:
	U first;
	V second;
};

int main(int argc, char* argv[])
{
	Pair<int, int> p;

	Pair<float, float> pf;

	Pair<string, int> ps;

	ps.first = "Hello";
	ps.second = 5;

	return 0;
}