#include <iostream>
using namespace std;

/*
enum (enumeration)
Scoped enum - preferred way to do enums
switch
namespace
*/

// Now a "scoped enum", so we have to specify the scope of each of these identifiers,
// but we avoid name collisions
enum class SuccessEnum
{
	FAIL,
	SUCCESS,
	UNKNOWN
};

enum MyOtherEnum
{
	GREAT,
	GOOD,
	FAIL
};

SuccessEnum DoSomeWork()
{
	cout << "Doing work..." << endl;
	return SuccessEnum::SUCCESS;
}



namespace EnemyStuff
{
	enum EnemyType
	{
		GOBLIN,
		ORC,
		TROLL,
		DEMON
	};

	class Enemy
	{
	public:
		int hitpoints;
		EnemyType type;
	};
}

class Goblin : public EnemyStuff::Enemy
{
public:
	Goblin()
	{
		hitpoints = 5;
		type = EnemyStuff::GOBLIN;  // Tip: If you ever do this, there's probably a better way!
	}
};

int main(int argc, char* argv[])
{

	// Enums can be converted to and from integers (because that's what they actually are)
	SuccessEnum myValue = SuccessEnum::SUCCESS;
	int myNum = (int)myValue;
	myValue = (SuccessEnum)myNum;
	

	SuccessEnum workResult = DoSomeWork();
	if (workResult == SuccessEnum::SUCCESS)
	{
		cout << "The work was successful!" << endl;
	}
	else if (workResult == SuccessEnum::FAIL)
	{
		cout << "The work failed..." << endl;
	}
	else if (workResult == SuccessEnum::UNKNOWN)
	{
		cout << "I'm not sure what happened." << endl;
	}
	else
	{
		//  Default if no match was made
	}


	myNum = 10;
	switch (myNum)
	{
	case 0:
		cout << "0" << endl;
		break;
	case 1:
		cout << "1" << endl;
		break;  // break is "optional", but then the execution will drop through to the next case's statements.
	case 9:
	case 10:
		cout << "This is 9 or 10" << endl;
		break;
	default:
		cout << "That value was not 0, 1, or 10." << endl;
		break;
	}

	switch (DoSomeWork())
	{
	case SuccessEnum::SUCCESS:
		cout << "The work was successful!" << endl;
		break;
	case SuccessEnum::FAIL:
		cout << "The work failed..." << endl;
		break;
	case SuccessEnum::UNKNOWN:
		cout << "I'm not sure what happened." << endl;
		break;
	}



	cin.get();
	return 0;
}