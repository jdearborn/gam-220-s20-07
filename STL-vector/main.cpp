#include <iostream>
#include <vector>
using namespace std;

int main(int argc, char* argv[])
{
	vector<int> myVector;

	myVector.push_back(5);
	myVector.push_back(23);
	myVector.push_back(4);

	cout << "Contents of the vector:" << endl;
	for (int i = 0; i < myVector.size(); ++i)
	{
		cout << myVector[i] << " ";
	}
	cout << endl;


	cout << "Traversal using an iterator:" << endl;
	for (vector<int>::iterator iter = myVector.begin(); iter != myVector.end(); ++iter)
	{
		cout << *iter << " ";
	}
	cout << endl;



	cin.get();
	return 0;
}