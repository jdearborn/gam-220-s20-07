#include <iostream>
using namespace std;

#define VERSION_NUMBER 3

// Pass-by-value / Value semantics
int Add5(int number)
{
	number += 5;
	return number;
}

// Pass-by-reference / Reference semantics
void Add10(int& number)
{
	number += 10;
}

int main(int argc, char* argv[])
{
	int result = Add5(7);

	cout << "Add5: " << result << endl;

	int input = 7;

	result = Add5(input);
	cout << "Input after Add5: " << input << endl;

	Add10(input);
	cout << "Input after Add10: " << input << endl;

	//Add10(8);
	// Error1: initial value of reference to non - const must be an lvalue
	// Error2: 'void Add10(int &)': cannot convert argument 1 from 'int' to 'int &'

	// Reference to an int (an alias)
	int& steve = input;
	steve += 16;  // `input` will be modified

	cout << "Version: " << VERSION_NUMBER << endl;

#ifdef OTHER_NUMBER
	cout << "Other number: " << OTHER_NUMBER << endl;
#endif

#if VERSION_NUMBER == 3
	cout << "Version is 3!" << endl;
#endif

#if VERSION_NUMBER == 5
	cout << "Version is 5!" << endl;
#endif

	cin.get();
	return 0;
}