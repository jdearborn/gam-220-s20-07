#include <iostream>
using namespace std;

void PrintArray(int* array, int size)
{
	for (int i = 0; i < size; ++i)
	{
		cout << array[i] << " ";
	}
	cout << endl;
}

void ResizeArray(int*& array, int size, int newSize)
{
	int* newArray = new int[newSize];
	int min = (size < newSize? size : newSize);
	for (int i = 0; i < min; ++i)
	{
		newArray[i] = array[i];
	}
	delete[] array;
	array = newArray;
}

int main(int argc, char* argv[])
{
	int stackArray[6];
	for (int i = 0; i < 6; ++i)
	{
		stackArray[i] = i + 1;
	}

	cout << "StackArray: " << endl;
	PrintArray(stackArray, 6);


	int* heapArray;
	heapArray = new int[10];

	for (int i = 0; i < 10; ++i)
	{
		heapArray[i] = i * 2;
	}

	cout << "HeapArray: " << endl;
	PrintArray(heapArray, 10);

	delete[] heapArray;

	// Bad!  We don't own that memory any more!
	//cout << heapArray[3] << endl;

	heapArray = nullptr;

	// Let's do it safely!
	if (heapArray != nullptr)
		cout << heapArray[3] << endl;

	heapArray = new int[4];

	// Copy values from stackArray into heapArray
	for (int i = 0; i < 4; ++i)
	{
		heapArray[i] = stackArray[i];
	}

	cout << "Heap array after copy: " << endl;
	PrintArray(heapArray, 4);

	// Let's "resize" the heapArray
	int* tempArray = new int[20];
	for (int i = 0; i < 4; ++i)
	{
		tempArray[i] = heapArray[i];
	}
	delete[] heapArray;
	heapArray = tempArray;

	///  Write a function, ResizeArray(), which returns void and 
	///  gives you back (somehow...) the array resized to a given length.

	PrintArray(heapArray, 20);

	//cout << std::hex << heapArray[7] << endl;
	// cout << std::dec;

	delete[] heapArray;

	cin.get();
	return 0;
}