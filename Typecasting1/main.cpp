#include <iostream>
using namespace std;

void MyFn(int i)
{
	cout << "That's an integer." << endl;
}

void MyFn(float f)
{
	cout << "That's a float." << endl;
}

int main(int argc, char* argv[])
{
	/* Typecasting */
	int i = 0;
	float f = (float)i;

	float g = float(i);

	/* The Modern C++ way */

	float h = static_cast<float>(i);

	/* To disambiguate a function call */

	MyFn(static_cast<float>(i));


	/* To force a type conversion */

	int grades[] = {90, 100, 78, 91};
	int count = 4;

	int sum = 0;
	for (int i = 0; i < count; ++i)
	{
		sum += grades[i];
	}

	float average = sum / (float)count;


	return 0;
}