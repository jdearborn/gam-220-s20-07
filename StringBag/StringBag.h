#pragma once
#include <string>
using std::string;

class StringBag
{
public:

	StringBag()
	{
		// Set up an empty StringBag that has room for 10 strings.
		capacity = 10;
		contents = new string[capacity];
		size = 0;
	}

	StringBag(int initialCapacity)
	{
		// Set up an empty StringBag that has room for `initialCapacity` strings.
		capacity = initialCapacity;
		contents = new string[capacity];
		size = 0;
	}

	// Adds a new string to the bag.  If the bag is already full, this does nothing.
	void Add(string newString)
	{
		if (size < capacity)
		{
			contents[size] = newString;
			size++;
		}
	}

private:
	string* contents;
	int size;
	int capacity;
};