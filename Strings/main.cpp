#include <iostream>
#include <string>
using namespace std;

int main(int argc, char* argv[])
{
	char myString[] = {'H', 'e', 'l', 'l', 'o', '!'};

	for (int i = 0; i < 6; ++i)
	{
		cout << myString[i];
	}

	cout << endl;


	const char* myString2 = "Hello!";  // Ends with a Null Terminator byte

	cout << myString2 << endl;

	char myString3[6];
	for (int i = 0; i < 6; ++i)
	{
		myString3[i] = myString2[i];
	}
	
	cout << "Without a Null byte: " << myString3 << endl;

	string myString4 = "This is a string object";

	cout << myString4 << endl;
	cout << "That string has a size of: " << myString4.size() << endl;

	// C#: get a line of input from the user
	// string input = Console.ReadLine();
	// cin.getline() <-- bad!
	getline(cin, myString4);

	cout << myString4 << endl;

	cin.get();
	return 0;
}