#ifndef __VECTOR_INL__
#define __VECTOR_INL__

#include "Vector.h"

template<typename T>
Vector<T>::Vector()
{
	// Set up an empty Vector that can contain up to 10 elements.
	capacity = 10;
	contents = new T[capacity];
	size = 0;
}

template<typename T>
Vector<T>::Vector(int initialCapacity)
{
	// Set up an empty Vector that can contain up to `initialCapacity` elements.
	capacity = initialCapacity;
	contents = new T[capacity];
	size = 0;
}

// Adds an integer to the vector.
template<typename T>
void Vector<T>::PushBack(T value)
{
	if (size == capacity)
	{
		Reserve(capacity + 1);
	}

	contents[size] = value;
	size++;
}

template<typename T>
void Vector<T>::Reserve(int newCapacity)
{
	if (capacity < newCapacity)
	{
		T* newArray = new T[newCapacity];
		for (int i = 0; i < size; ++i)
		{
			newArray[i] = contents[i];
		}
		delete[] contents;
		contents = newArray;
		capacity = newCapacity;
	}
}

template<typename T>
void Vector<T>::Resize(int newSize)
{
	if (newSize < size)
	{
		size = newSize;
	}
	else if (newSize > capacity)
	{
		Reserve(newSize);
		size = newSize;
	}
}

template<typename T>
int Vector<T>::Size()
{
	return size;
}

template<typename T>
T& Vector<T>::At(int index)
{
	return contents[index];
}

#endif
