#include "Goblin.h"
#include <iostream>
using namespace std;

Goblin::Goblin()
{
	hitpoints = 6;
	attackPower = 1;
}

void Goblin::TakeDamage(int damage)
{
	cout << "Yelp!" << endl;
	hitpoints -= damage;
}