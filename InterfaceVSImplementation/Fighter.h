#pragma once

class Fighter
{
public:
	int hitpoints;
	int attackPower;

	Fighter();

	void TakeDamage(int damage);
};