#include "Fighter.h"
#include <iostream>
using namespace std;

Fighter::Fighter()
{
	hitpoints = 10;
	attackPower = 2;
}

void Fighter::TakeDamage(int damage)
{
	cout << "Ouch!" << endl;
	hitpoints -= damage;
}