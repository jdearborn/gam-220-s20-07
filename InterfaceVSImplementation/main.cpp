#include <iostream>
#include "Fighter.h"
#include "Goblin.h"
using namespace std;

int main(int argc, char* argv[])
{
	Fighter f;
	Goblin g;

	while (f.hitpoints > 0 && g.hitpoints > 0)
	{
		g.TakeDamage(f.attackPower);
		f.TakeDamage(g.attackPower);
	}

	if (f.hitpoints > 0)
	{
		cout << "The fighter has lived.  He has " << f.hitpoints << " hitpoints left." << endl;
	}
	if (g.hitpoints > 0)
	{
		cout << "The goblin has lived.  He has " << g.hitpoints << " hitpoints left." << endl;
	}

	return 0;
}