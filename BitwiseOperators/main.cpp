#include <iostream>
#include <bitset>
using namespace std;

int main(int argc, char* argv[])
{
	// bitwise AND...  &
	unsigned char c =     0b00001010;
	unsigned char d = c & 0b00000100;
	// expected result:     00000100

	// bitwise OR...  |
	unsigned char e =	  0b00010010
						| 0b01100110;
	// expected result:     01110110

	// bitwise XOR...  ^  (exclusive OR)
	unsigned char f =	  0b00010010
						| 0b01100110;
	// expected result:     01110100

	// bitwise NOT...  ~
	unsigned char g =   ~0b00010010;
	// expected result:    11101101

	// right-shift (shifts down X times)
	unsigned char h = 0b00010010 >> 1;
	// expected result: 00001001
	h =               0b00010010 >> 2;
	// expected result: 00000100

	// left-shift (shifts up X times)
	unsigned char i = 0b00010010 << 1;
	// expected result: 00100100

	// Compound assignment for most of these
	//b &= 17;

	cout << bitset<8>(i) << endl;


	return 0;
}