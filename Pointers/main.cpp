#include <iostream>
using namespace std;

int main(int argc, char* argv[])
{
	int bob = 5;
	
	// A pointer is a memory address
	int* bobsAddress = &bob;  // Address-of operator

	cout << "Bob: " << bob << endl;

	// Dereference a pointer to access what is stored there
	cout << "This is Bob's address: " << bobsAddress << endl;
	cout << "Bob via memory address: " << *bobsAddress << endl;

	*bobsAddress = 17;

	int definitelyNotBob = 10;

	bobsAddress = &definitelyNotBob;

	int* anotherIntPointer = new int;
	*anotherIntPointer = 23;
	delete anotherIntPointer;  // Always match 'new' with a 'delete'.

	// Don't do this!  Memory leak...
	/*while (true)
	{
		int* ptr = new int;
	}*/


	cin.get();
	return 0;
}