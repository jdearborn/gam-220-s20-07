#include <iostream>
using namespace std;

int main(int argc, char* argv[])
{
	// Pointer arithmetic: Directly modifying memory addresses

	int* myArray = new int[10];
	for (int i = 0; i < 10; ++i)
	{
		myArray[i] = i * 3;
	}

	for (int i = 0; i < 10; ++i)
	{
		cout << myArray[i] << " ";
	}
	cout << endl;



	int* a = myArray;

	cout << "Starting value of `a`: " << *a << endl;
	cout << "Memory address (value) of `a`: " << a << endl;

	// Pointer arithmetic: Add one to `a`...
	a++;

	cout << "Second value of `a`: " << *a << endl;
	cout << "Memory address: " << a << endl;

	a += 2;

	cout << "Fourth value of `a`: " << *a << endl;
	cout << "Memory address: " << a << endl;




	cout << "Printing a region of memory using pointer arithmetic..." << endl;
	int* start = myArray;
	int* end = myArray + 10;
	for (int* p = start; p != end; ++p)
	{
		cout << *p << " ";
	}
	cout << endl;


	int x = 0;

	while (true)
	{
		char c = cin.get();
		if (c == 'a')
			--x;
		else if (c == 'd')
			++x;
		cout << "Hello" + x << endl;
	}




	delete[] myArray;

	cin.get();
	return 0;
}