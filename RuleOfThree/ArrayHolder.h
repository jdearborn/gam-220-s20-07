#pragma once

// Example of RAII: Resource Acquisition Is Initialization
class ArrayHolder
{
public:
	ArrayHolder()
	{
		array = new int[50];
		size = 50;
	}
	// Copy constructor
	ArrayHolder(const ArrayHolder& other)
	{
		size = other.Size();

		array = new int[size];
		for (int i = 0; i < size; ++i)
		{
			array[i] = other.array[i];
		}
	}

	// Rule of Three
	// If you implement a destructor, a copy constructor, or a copy assignment operator, you should do all three.
	~ArrayHolder()
	{
		delete[] array;
	}

	// Copy assignment operator
	ArrayHolder& operator=(const ArrayHolder& other)
	{
		size = other.size;
		delete[] array;
		array = new int[size];
		for (int i = 0; i < size; ++i)
		{
			array[i] = other.array[i];
		}

		return *this;
	}

	int& operator[](int index)
	{
		return array[index];
	}

	int Size() const
	{
		return size;
	}

private:
	int* array;
	int size;
};