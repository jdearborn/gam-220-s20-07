#include <iostream>
#include <fstream>
#include <string>
using namespace std;

class Record
{
public:
	string name;
	int level;
	int hitpoints;
	int gold;
};

int main(int argc, char* argv[])
{
	ifstream fin("some_data.txt");

	while (fin.good())
	{
		string input;
		getline(fin, input);  // skip record label
		getline(fin, input);  // get name

		Record record;
		record.name = input;
		fin >> record.level;
		fin >> record.hitpoints;
		fin >> record.gold;

		fin.ignore(numeric_limits<streamsize>::max(), '\n');

		cout << record.name << " has " << record.gold << " gold." << endl;
	}


	return 0;
}