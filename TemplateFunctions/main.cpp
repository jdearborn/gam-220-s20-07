#include <iostream>
using namespace std;

template<typename T>
T GetMax(T a, T b)
{
	if (a > b)
		return a;
	return b;
}

template<>
int GetMax(int a, int b)
{
	cout << "Hey" << endl;
	return a;
}

int main(int argc, char* argv[])
{
	int a = 4;
	int b = 6;

	int c = GetMax(a, b);

	cout << "The greater value is: " << c << endl;

	return 0;
}