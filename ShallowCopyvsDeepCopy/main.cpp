#include <iostream>
using namespace std;

int main(int argc, char* argv[])
{
	int a = 0;
	float b = 0.0f;

	// Plain-Old Data (POD)
	// Copy with the assignment operator
	a = 5;


	// How about some non-POD types?
	int stackArray[10];
	int* array1;
	array1 = new int[10];

	// Store values from 0 to 27, counting by 3
	for (int i = 0; i < 10; ++i)
	{
		array1[i] = i * 3;
	}
	


	// This is a shallow copy: Just the memory address has been copied, not any of the elements of the array.
	int* array2 = array1;


	array1[3] = 999;

	cout << "Printing array2..." << endl;
	for (int i = 0; i < 10; ++i)
	{
		cout << array2[i] << " ";
	}
	cout << endl;



	// Let's do a deep copy instead.
	// Deep copies make a complete, separate clone of the original data.
	int* array3 = new int[10];
	for (int i = 0; i < 10; ++i)
	{
		array3[i] = array1[i];
	}

	array1[4] = 1234;

	cout << "Printing array3: " << endl;
	for (int i = 0; i < 10; ++i)
	{
		cout << array3[i] << " ";
	}
	cout << endl;

	cout << "Printing array1: " << endl;
	for (int i = 0; i < 10; ++i)
	{
		cout << array1[i] << " ";
	}
	cout << endl;


	delete[] array1;
	delete[] array3;



	cin.get();
	return 0;
}