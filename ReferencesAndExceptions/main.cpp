#include <iostream>
#include <stdexcept>
#include <string>
using namespace std;

// Pass by value
void PassByValue(int value)
{
	// Changes to `value` do not persist beyond the lifetime of this function
	value = 1;
}

// Pass by reference (&)
void PassByReference(int& refVariable)
{
	// Changes to `refVariable` do persist: They actually change the variable that was passed in.
	refVariable = 2;
}

// Pass by reference (pointer)
void PassByReferenceUsingPointer(int* ptr)
{
	// If there's a problem/error, what do we do?

	// Could do nothing...  Let the program continue on its way.
	/*if (ptr == nullptr)
	{
		return;
	}*/

	// Could print a message or otherwise communicate to the user that something went wrong.
	/*if (ptr == nullptr)
	{
		cout << "Hey, that's a null pointer and I don't like it!" << endl;
		return;
	}*/

	// Could try to "fix" the problem and continue on...
	/*int hackyFix = 0;
	if (ptr == nullptr)
	{
		ptr = &hackyFix;
	}*/

	// Could throw an exception
	if (ptr == nullptr)
	{
		//throw 1234;
		throw runtime_error("PassByReferenceUsingPointer(): ptr was null");
	}


	*ptr = 3;
}


int& GetValueAtIndex(int* array, int size, int index)
{
	if (array == nullptr)
	{
		throw invalid_argument("GetValueAtIndex() was given a null array!");
	}
	if (index >= size)
	{
		throw out_of_range("GetValueAtIndex() was given an index beyond the end of the array!");
	}
	if (index < 0)
	{
		throw out_of_range("GetValueAtIndex() was given a negative index!");
	}


	return array[index];
}


// Pass by value: Makes a copy of the string passed in.
void PrintString(string s)
{
	cout << s << endl;
}

// Pass by reference: No copy made...  but...
void PrintStringRef(string& s)
{
	cout << s << endl;

	// ...but what if it modifies the string?
	s += " Ha ha!  You've been modified.";
}

// Pass by constant reference: No copy made for variables, copy made for literals.  No modification allowed.
void PrintStringConstRef(const string& s)
{
	cout << s << endl;
}



int main(int argc, char* argv[])
{
	PassByValue(56);

	int myInt = 5;
	PassByReference(myInt);

	int& myIntReference = myInt;  // Alias

	int* myIntPtr = new int;
	PassByReferenceUsingPointer(&myInt);
	PassByReferenceUsingPointer(myIntPtr);

	try
	{
		PassByReferenceUsingPointer(nullptr);
	}
	catch (int errorCode)
	{
		cout << "Hey, an exception was thrown.  Error code: " << errorCode << endl;

		if (errorCode == 4567)
			throw;
	}
	catch (exception& e)
	{
		cout << "An exception occurred: " << e.what() << endl;
	}


	delete myIntPtr;



	// More with references

	int array[] = {78, 54, 109};
	cout << "Value at index 1: " << GetValueAtIndex(array, 3, 1) << endl;

	// Weird, but we can do this because this reference represents the actual integer variable in that array.
	GetValueAtIndex(array, 3, 1) = 14;
	cout << "Value at index 1: " << GetValueAtIndex(array, 3, 1) << endl;


	try
	{
		GetValueAtIndex(nullptr, 3, 1);

		GetValueAtIndex(array, 3, 8790);

		GetValueAtIndex(array, 3, -23);
	}
	catch (exception& e)
	{
		cout << "Yep, that's an exception: " << e.what() << endl;
	}
	

	PrintString("This is a string.");
	PrintString("This is a BIG string  ILAJSDLK JASld ajsdj wl;dk slkfj salkfj alksjf asl;kfj alksd "
	"sadf jlasfj laksjfd ;lkajdsf;k dfkl jslkenw foialj kma"
	"la ijslfij laisfj lasjflksjdf;ajeso;ih naskldfh; lksdjf s"
	"kjsdf;sdjf;oi asdjf lkdjf al;sdfji asdjflkaj sdlfk");

	string myString = "I can make a reference to this variable.";
	PrintStringRef(myString);

	PrintStringConstRef(myString);
	PrintStringConstRef("Here's a string literal.");


	cin.get();
	return 0;
}